<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Sequence\Service;

use Inisiatif\Component\Contract\Sequence\Model\SequenceInterface;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface SequenceServiceInterface
{
    /**
     * @param SequenceInterface $sequence
     *
     * @return SequenceInterface
     */
    public function save(SequenceInterface $sequence): SequenceInterface;

    /**
     * @param string $code
     *
     * @return SequenceInterface
     */
    public function initiation(string $code): SequenceInterface;

    /**
     * @param SequenceInterface $sequence
     *
     * @return bool
     */
    public function increment(SequenceInterface $sequence): bool;
}
