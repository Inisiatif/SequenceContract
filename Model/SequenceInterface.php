<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Sequence\Model;

use DateTimeInterface;
use Inisiatif\Component\Contract\Resource\Model\ResourceInterface;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface SequenceInterface extends ResourceInterface
{
    /**
     * @return string|null
     */
    public function getCode(): ?string;

    /**
     * @param string|null $value
     * @return self|SequenceInterface
     */
    public function setCode(?string $value): self;

    /**
     * @return DateTimeInterface|null
     */
    public function getDate(): ?DateTimeInterface;

    /**
     * @param DateTimeInterface|null $value
     * @return self|SequenceInterface
     */
    public function setDate(?DateTimeInterface $value): self;

    /**
     * @return int
     */
    public function getLastSequence(): int;

    /**
     * @param int $value
     * @return self|SequenceInterface
     */
    public function setLastSequence(int $value): self;
}
