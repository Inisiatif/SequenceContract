<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Sequence\Repository;

use DateTimeInterface;
use Inisiatif\Component\Contract\Sequence\Model\SequenceInterface;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface SequenceRepositoryInterface
{
    /**
     * @param string                 $code
     * @param DateTimeInterface|null $date
     *
     * @return SequenceInterface|null
     */
    public function fetchOneByCode(string $code, ?DateTimeInterface $date): ?SequenceInterface;

    /**
     * @param string                 $code
     * @param DateTimeInterface|null $date
     *
     * @return SequenceInterface
     */
    public function fetchOneOrCreateByCode(string $code, ?DateTimeInterface $date): SequenceInterface;
}
