<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Sequence\Util;

use Inisiatif\Component\Contract\Sequence\Model\SequenceInterface;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface SequenceGeneratorInterface
{
    /**
     * @param SequenceInterface $sequence
     * @param string|null $replace
     * @param string|null $prefix
     * @param bool|null $withDatetime
     * @return string
     */
    public static function generate(SequenceInterface $sequence, ?string $replace, ?string $prefix, ?bool $withDatetime = true): string;
}
